//
//  CanvasView.h
//  PaintDemo
//
//  Created by mun on 8/20/15.
//  Copyright © 2015 mun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaletteView.h"
@interface CanvasView : UIView <PaletteViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView* imgView;
@property float red, green, blue, alpha;
@property int brushSize;
@property BOOL erasing;
@end
