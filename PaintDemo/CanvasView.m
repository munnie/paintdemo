//
//  CanvasView.m
//  PaintDemo
//
//  Created by mun on 8/20/15.
//  Copyright © 2015 mun. All rights reserved.
//

#import "CanvasView.h"

@implementation CanvasView {
    CGPoint lastPoint;
    NSMutableArray* stateArray;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)awakeFromNib
{
    stateArray = @[].mutableCopy;
    [self paletteViewDidSelectDelete];
}

#pragma mark-- PaletteViewDelegate
- (void)paletteViewDidSelectColor:(UIColor*)color
{
    _erasing = NO;
    CGFloat red = 0.0, green = 0.0, blue = 0.0, alpha = 0.0;
    [color getRed:&red green:&green blue:&blue alpha:&alpha];
    _red = red;
    _green = green;
    _blue = blue;
    _alpha = alpha;
}

- (void)paletteViewDidSelectBrushSize:(float)size
{
    _brushSize = size;
}

- (void)paletteViewDidSelectDelete
{
    UIGraphicsBeginImageContext(self.frame.size);

    CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeClear);

    [_imgView.image drawInRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
    CGContextSetLineWidth(UIGraphicsGetCurrentContext(), _brushSize);
    CGContextFillRect(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, self.frame.size.width, self.frame.size.height));
    _imgView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
}

- (void)paletteViewDidSelectErase
{
    _erasing = YES;
}

- (void)paletteViewDidSelectUndo
{
    if ([stateArray count] > 0) {
        _imgView.image = [stateArray lastObject];
        [stateArray removeLastObject];
    }
}

#pragma mark-- Drawing
- (UIView*)hitTest:(CGPoint)point withEvent:(UIEvent*)event
{
    if (CGRectContainsPoint(self.frame, point))
        return self;
    return nil;
}

- (void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
{
    if (_imgView.image) {
        [stateArray addObject:_imgView.image];
    }

    if ([stateArray count] > 10) {
        [stateArray removeObjectAtIndex:0];
    }
    UITouch* touch = [touches anyObject];
    lastPoint = [touch locationInView:self];
}

- (void)touchesMoved:(NSSet*)touches withEvent:(UIEvent*)event
{

    UITouch* touch = [touches anyObject];
    CGPoint currentPoint = [touch locationInView:self];
    UIGraphicsBeginImageContext(self.frame.size);

    if (_erasing)
        CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeClear);

    [_imgView.image drawInRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
    CGContextSetLineWidth(UIGraphicsGetCurrentContext(), _brushSize);
    CGContextBeginPath(UIGraphicsGetCurrentContext());
    CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), _red, _green, _blue, _alpha);
    CGContextMoveToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
    CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), currentPoint.x, currentPoint.y);
    CGContextStrokePath(UIGraphicsGetCurrentContext());
    _imgView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    lastPoint = currentPoint;
}

- (void)touchesEnded:(NSSet*)touches withEvent:(UIEvent*)event
{
    UITouch* touch = [touches anyObject];
    CGPoint currentPoint = [touch locationInView:self];
    UIGraphicsBeginImageContext(self.frame.size);

    if (_erasing)
        CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeClear);

    [_imgView.image drawInRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
    CGContextSetLineWidth(UIGraphicsGetCurrentContext(), _brushSize);
    CGContextBeginPath(UIGraphicsGetCurrentContext());
    CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), _red, _green, _blue, _alpha);
    CGContextMoveToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
    CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), currentPoint.x, currentPoint.y);
    CGContextStrokePath(UIGraphicsGetCurrentContext());
    _imgView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    lastPoint = currentPoint;
}


@end
