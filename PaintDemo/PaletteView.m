//
//  PaletteView.m
//  PaintDemo
//
//  Created by mun on 8/20/15.
//  Copyright © 2015 mun. All rights reserved.
//

#import "PaletteView.h"

@implementation PaletteView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}*/

- (void)awakeFromNib
{
    self.layer.cornerRadius = 10;

    NSArray* arrayBtn = @[ _btnColorBlue, _btnColorGreen, _btnColorRed, _btnColorViolet, _btnColorYellow, _btnDelete, _btnErase, _btnPenBig, _btnPenSmall, _btnUndo ];
    for (UIButton* bt in arrayBtn) {
        bt.layer.cornerRadius = 5;
        bt.layer.borderWidth = 2;
        bt.layer.borderColor = [UIColor clearColor].CGColor;
    }

    [self selectColor:_btnColorRed];
    [self selectBrush:_btnPenSmall];
}

- (IBAction)selectColor:(UIButton*)selectColorButton
{
    NSArray* arrayBtn = @[ _btnColorBlue, _btnColorGreen, _btnColorRed, _btnColorViolet, _btnColorYellow, _btnErase ];
    for (UIButton* bt in arrayBtn) {
        bt.layer.borderColor = [UIColor clearColor].CGColor;
    }

    selectColorButton.layer.borderColor = [UIColor redColor].CGColor;

    if (selectColorButton == self.btnErase) {
        if ([_delegate respondsToSelector:@selector(paletteViewDidSelectErase)])
            [_delegate paletteViewDidSelectErase];
    }
    else {
        UIColor* selectedColor;
        if (selectColorButton == self.btnColorRed) {
            selectedColor = [UIColor colorWithRed:221 / 255.0 green:70 / 255.0 blue:82 / 255.0 alpha:1];
        }
        if (selectColorButton == self.btnColorBlue) {
            selectedColor = [UIColor colorWithRed:34 / 255.0 green:142 / 255.0 blue:164 / 255.0 alpha:1];
        }
        if (selectColorButton == self.btnColorGreen) {
            selectedColor = [UIColor colorWithRed:153 / 255.0 green:185 / 255.0 blue:47 / 255.0 alpha:1];
        }
        if (selectColorButton == self.btnColorViolet) {
            selectedColor = [UIColor colorWithRed:155 / 255.0 green:137 / 255.0 blue:179 / 255.0 alpha:1];
        }
        if (selectColorButton == self.btnColorYellow) {
            selectedColor = [UIColor colorWithRed:239 / 255.0 green:170 / 255.0 blue:52 / 255.0 alpha:1];
        }

        if ([_delegate respondsToSelector:@selector(paletteViewDidSelectColor:)])
            [_delegate paletteViewDidSelectColor:selectedColor];
    }
}

- (IBAction)selectBrush:(UIButton*)selectBrushButton
{
    NSArray* arrayBtn = @[ _btnPenSmall, _btnPenBig ];
    for (UIButton* bt in arrayBtn) {
        bt.layer.borderColor = [UIColor clearColor].CGColor;
    }

    selectBrushButton.layer.borderColor = [UIColor redColor].CGColor;

    float size = 0;
    if (selectBrushButton == self.btnPenSmall) {
        size = 5;
    }
    if (selectBrushButton == self.btnPenBig) {
        size = 15;
    }

    if ([_delegate respondsToSelector:@selector(paletteViewDidSelectBrushSize:)])
        [_delegate paletteViewDidSelectBrushSize:size];
}

- (IBAction)undo
{
    if ([_delegate respondsToSelector:@selector(paletteViewDidSelectUndo)])
        [_delegate paletteViewDidSelectUndo];
}

- (IBAction) delete
{
    if ([_delegate respondsToSelector:@selector(paletteViewDidSelectDelete)])
        [_delegate paletteViewDidSelectDelete];
}

@end
