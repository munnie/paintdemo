//
//  PaintViewController.h
//  PaintDemo
//
//  Created by mun on 8/20/15.
//  Copyright © 2015 mun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CanvasView.h"
@interface PaintViewController : UIViewController
@property (weak, nonatomic) IBOutlet PaletteView* paletteView;
@property (weak, nonatomic) IBOutlet CanvasView* canvasView;
@end
