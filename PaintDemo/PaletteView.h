//
//  PaletteView.h
//  PaintDemo
//
//  Created by mun on 8/20/15.
//  Copyright © 2015 mun. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PaletteViewDelegate <NSObject>

- (void)paletteViewDidSelectColor:(UIColor*)color;
- (void)paletteViewDidSelectBrushSize:(float)size;
- (void)paletteViewDidSelectErase;
- (void)paletteViewDidSelectUndo;
- (void)paletteViewDidSelectDelete;

@end

@interface PaletteView : UIView
@property (weak, nonatomic) IBOutlet UIButton* btnColorRed;
@property (weak, nonatomic) IBOutlet UIButton* btnColorGreen;
@property (weak, nonatomic) IBOutlet UIButton* btnColorBlue;
@property (weak, nonatomic) IBOutlet UIButton* btnColorViolet;
@property (weak, nonatomic) IBOutlet UIButton* btnColorYellow;

@property (weak, nonatomic) IBOutlet UIButton* btnErase;
@property (weak, nonatomic) IBOutlet UIButton* btnPenSmall;
@property (weak, nonatomic) IBOutlet UIButton* btnPenBig;

@property (weak, nonatomic) IBOutlet UIButton* btnUndo;
@property (weak, nonatomic) IBOutlet UIButton* btnDelete;

@property IBOutlet id<PaletteViewDelegate> delegate;
@end
